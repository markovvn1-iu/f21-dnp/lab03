# Lab 03

Not so simple client-server file transfer

### Download and create venv

1. Download repo: `git clone https://gitlab.com/markovvn1-iu/f21-dnp/lab03.git`
2. Open downloaded folder: `cd lab03`
3. Create python virtual environment: `make venv`

### Main task (send files)

1. Start server: `make file_server_up`
2. Run client: `make file_run_client`
3. Have fun!