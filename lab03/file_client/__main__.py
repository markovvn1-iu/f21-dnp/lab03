import argparse
from loguru import logger

from .client import FileTransferClient, FileTransferClientData


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="File sender client",
        add_help=True,
    )

    parser.add_argument("file_name", type=str, metavar="FILE_NAME", help="name of file to send")
    parser.add_argument("-addr", type=str, metavar="ADDR", default='localhost', help="address of the server")
    parser.add_argument("-port", type=int, metavar="PORT", default=37713, help="port of the server")
    return parser.parse_args(args)


def get_file_extension(file_name):
    i = file_name.rfind('.')
    return file_name[i+1:] if i >= 0 else ''


def main():
    """Main entrypoint."""
    args = parse_args()

    try:
        with open(args.file_name, 'rb') as f:
            file_data = f.read()
    except BaseException as e:
        logger.critical(repr(e))
        return

    msg = FileTransferClientData(
        size=len(file_data),
        extension=get_file_extension(args.file_name),
        data=file_data,
    )

    client = FileTransferClient(args.addr, args.port, msg)
    try:
        client.send_file()
    except ConnectionError as e:
        logger.critical(repr(e))
        return

    

if __name__ == "__main__":
    main()
