import socket
from typing import Tuple
from loguru import logger
import struct
import math
import time


from dataclasses import dataclass


@dataclass(frozen=True)
class FileTransferClientData:
    size: int
    extension: str
    data: bytes


class FileTransferClient:

    data: FileTransferClientData
    addr: Tuple[str, int]
    _buffersize: int = 1024
    _max_data_size: int = 1000
    _socket: socket.socket
    _start_seqno: int = 0
    _end_seqno: int = 0

    def __init__(self, host: str, port: int, data: FileTransferClientData):
        logger.info('Start client on {}', (host, port))
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.settimeout(0.5)
        self._start_seqno = 0
        self.addr = (host, port)
        self.data = data

    def _send_start_message(self) -> int:
        ext = self.data.extension.encode()[:32]
        ext += b'\0' * (32 - len(ext))
        
        start_msg = b'S' + struct.pack('<II', self._start_seqno, self.data.size) + ext

        for _ in range(5):
            logger.debug('Send start message to server')
            self._socket.sendto(start_msg, self.addr)

            try:
                ack_msg, _ = self._socket.recvfrom(9)
            except socket.timeout:
                continue

            if len(ack_msg) == 9 and ack_msg[:1] == b'A':
                logger.debug('Recive ack message for start message')
                self._start_seqno, self._buffersize = struct.unpack('<II', ack_msg[1:])
                self._max_data_size = self._buffersize - 1 - 4
                logger.debug('Set buffersize={} and max_data_size={}', self._buffersize, self._max_data_size)
                self._end_seqno = self._start_seqno + math.ceil(self.data.size / self._max_data_size)
                return self._start_seqno
        
        raise ConnectionError('Cannot connect to the server')

    def _send_data_message(self, seqno: int) -> int:
        i = (seqno - self._start_seqno) * self._max_data_size
        data_msg = b'D' + struct.pack('<I', seqno) + self.data.data[i:i + self._max_data_size]
        assert len(data_msg) <= self._buffersize

        for _ in range(5):
            logger.debug('Send data message to server, seqno={}', seqno)
            self._socket.sendto(data_msg, self.addr)

            try:
                ack_msg, _ = self._socket.recvfrom(5)
            except socket.timeout:
                continue

            if len(ack_msg) == 5 and ack_msg[:1] == b'A':
                next_seqno, = struct.unpack('<I', ack_msg[1:])
                logger.debug('Recive ack message for data message, seqno={}', next_seqno)
                return next_seqno
        
        raise ConnectionError('Cannot connect to the server')

    def send_file(self) -> None:
        seqno = self._send_start_message()

        while seqno != self._end_seqno:
            seqno = self._send_data_message(seqno)
            time.sleep(0.01)
