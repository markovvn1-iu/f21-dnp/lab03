import time
from typing import Any, List, Tuple
from loguru import logger


class ItemHolder:
    "Keep element for some time, the delete"

    _dataset: List[Tuple[int, Any]] = []

    def keep(self, item: Any, keep_for: float) -> None:
        """Keep element for `keep_for` seconds

        Args:
            item (Any): element you want to keep
            keep_for (float): amount of seconds system will keep element
        """
        expire_time = time.perf_counter_ns() + round(keep_for * 1e9)
        self._dataset.append((expire_time, item))
        logger.debug('Item was added to item holder')

    def update(self) -> None:
        c_time = time.perf_counter_ns()
        size_before = len(self._dataset)
        self._dataset = [item for item in self._dataset if item[0] > c_time]

        deleted_count = size_before - len(self._dataset)
        if deleted_count > 0:
            logger.debug('Delete {} items from item holder', deleted_count)
