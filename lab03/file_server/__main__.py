import argparse
from loguru import logger


from .item_holder import ItemHolder
from .server import FileTransferServer, FileTransferServerClientData


BUFFER_SIZE = 1000


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Calculator server",
        add_help=True,
    )

    parser.add_argument("-port", type=int, metavar="PORT", default=37713, help="port for start server")
    return parser.parse_args(args)

"""
The server should hold the information related to the successfully finished file reception for some
time (1.0s) before finally removing it.

So, let's use ItemHolder!
"""
itemHolder = ItemHolder()

def on_recive_file(client_data: FileTransferServerClientData) -> None:
    itemHolder.keep(client_data, 1)  # save recived data for 1 second


def main():
    """Main entrypoint."""
    args = parse_args()
    server = FileTransferServer(args.port, on_recive_file)
    server.set_buffersize(BUFFER_SIZE)

    try:
        while True:
            server.spin_once()
            itemHolder.update()
        # file_name = f"new_{message_metadata.name}"
        # logger.debug('Save data to file {}', file_name)
        # with open(file_name, 'wb') as f:
        #     f.write(file_data)
        # logger.info('Recived file is saved to {}', file_name)

    except KeyboardInterrupt:
        logger.error('KeyboardInterrupt. Stop server')
        pass

if __name__ == "__main__":
    main()
