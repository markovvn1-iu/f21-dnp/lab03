from typing import Callable, Dict, List, Optional, Tuple
from loguru import logger
import socket
import struct
import time
from dataclasses import dataclass


@dataclass(frozen=True)
class FileTransferServerClientData:
    addr: Tuple[str, int]

    size: int
    extension: str
    data: bytes

        
class FileTransferServerClient:

    addr: Tuple[str, int]
    tstamp_ns: int = 0
    next_seqno: int = -1

    expected_size: int = -1
    current_size: int = 0
    extension: str = ''

    _recive_start_message: bool = False
    _raw_data: List[bytes] = []

    def __init__(self, addr: Tuple[str, int]):
        self.addr = addr

    def is_done(self) -> bool:
        return self.current_size == self.expected_size

    def recive_start_message(self) -> bool:
        return self._recive_start_message

    @staticmethod
    def is_start_message(msg: bytes) -> bool:
        return len(msg) == 41 and msg[:1] == b'S'

    @staticmethod
    def is_data_message(msg: bytes) -> bool:
        return len(msg) > 5 and msg[:1] == b'D'

    def process_start_message(self, msg: bytes) -> None:
        if not FileTransferServerClient.is_start_message(msg):
            raise ValueError('Start message has incorrect format')
        
        self.tstamp_ns = time.perf_counter_ns()

        self._raw_data = []
        self.current_size = 0
        self.next_seqno, self.expected_size = struct.unpack('<II', msg[1:9])
        ext = msg[9:]
        i = ext.find(b'\0')
        self.extension = (ext[:i] if i >= 0 else ext).decode()
        self._recive_start_message = True

        self.next_seqno += 1

    def process_data_message(self, msg: bytes) -> None:
        if not FileTransferServerClient.is_data_message(msg):
            raise ValueError('Data message has incorrect format')

        next_seqno, = struct.unpack('<I', msg[1:5])
        if self.next_seqno != next_seqno:
            logger.debug('Skip data message because is has incorrect seqno: {} (expected {})', next_seqno, self.next_seqno)
            return

        self.tstamp_ns = time.perf_counter_ns()

        data = msg[5:]
        self.current_size += len(data)
        self._raw_data.append(data)

        self.next_seqno += 1

    def get_data(self):
        return FileTransferServerClientData(addr=self.addr, size=self.current_size, extension=self.extension, data=b''.join(self._raw_data))



class FileTransferServer:

    _buffersize: int = 1024
    _socket: socket.socket
    _clients: Dict[Tuple[str, int], FileTransferServerClient] = {}
    _callback: Optional[Callable[[FileTransferServerClientData], None]] = None

    def __init__(self, port: int, callback: Callable[[FileTransferServerClientData], None]):
        self._callback = callback
        logger.info('Start server on port {}', port)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind(('0.0.0.0', port))
        self._socket.settimeout(0.1)

    def set_buffersize(self, buffersize: int) -> None:
        self._buffersize = buffersize

    def _process_client_message(self, client: FileTransferServerClient, msg: bytes) -> None:
        if FileTransferServerClient.is_start_message(msg):
            logger.debug('Client (addr={}) send start message', client.addr)
            client.process_start_message(msg)
            ack_message = b'A' + struct.pack('<II', client.next_seqno, self._buffersize)
        elif FileTransferServerClient.is_data_message(msg):
            logger.debug('Client (addr={}) send data message', client.addr)
            client.process_data_message(msg)
            ack_message = b'A' + struct.pack('<I', client.next_seqno)
        elif client.recive_start_message():
            logger.warning('Client (addr={}) send some strange data. Ignore it', client.addr)
            ack_message = b'A' + struct.pack('<I', client.next_seqno)
        else:
            logger.warning('New client (addr={}) send some strange data. Ignore it', client.addr)
            return

        self._socket.sendto(ack_message, client.addr)

    def _chech_all_clients(self):
        too_old_time = time.perf_counter_ns() - 3_000_000_000

        for cli_addr in list(self._clients.keys()):
            cli = self._clients[cli_addr]
            if cli.is_done():
                self._clients.pop(cli_addr)
                logger.debug('Client (addr={}) send all data. Call callback function', cli_addr)
                assert self._callback is not None
                self._callback(cli.get_data())

            if cli.tstamp_ns < too_old_time:
                logger.debug('Client (addr={}) do not send any data for 3 second. Delete client data', cli_addr)
                self._clients.pop(cli_addr)

    def spin_once(self) -> None:
        try:
            data, addr = self._socket.recvfrom(self._buffersize)
        except socket.timeout:
            self._chech_all_clients()
            return

        try:
            if addr in self._clients:
                self._process_client_message(self._clients[addr], data)
            elif FileTransferServerClient.is_start_message(data):
                new_client = FileTransferServerClient(addr)
                self._clients[addr] = new_client
                self._process_client_message(new_client, data)
            else:
                logger.warning('Client (addr={}) send some strange data. Expected start message', addr)
        except ValueError as e:
            logger.warning('Client connection failed with error: ' + repr(e))

        self._chech_all_clients()
